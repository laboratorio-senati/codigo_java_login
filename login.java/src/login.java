import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class login {
    public static void main(String[] args) {
        String url = "jdbc:mysql://localhost:3306/login";
        String user = "root";
        String password = "jhojan";

        try (Connection connection = DriverManager.getConnection(url, user, password)) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Bienvenido al sistema de registro e inicio de sesión.");
            System.out.println("1. Crear cuenta");
            System.out.println("2. Iniciar sesión");
            System.out.print("Seleccione una opción: ");
            int choice = scanner.nextInt();
            scanner.nextLine(); // Consumir la nueva línea

            if (choice == 1) {
                System.out.print("Ingrese un nombre de usuario: ");
                String newUsername = scanner.nextLine();
                System.out.print("Ingrese una contraseña: ");
                String newPassword = scanner.nextLine();

                if (!isUserRegistered(connection, newUsername)) {
                    registerUser(connection, newUsername, newPassword);
                } else {
                    System.out.println("La cuenta ya está registrada con estos datos.");
                }
            } else if (choice == 2) {
                System.out.print("Ingrese su nombre de usuario: ");
                String username = scanner.nextLine();
                System.out.print("Ingrese su contraseña: ");
                String inputPassword = scanner.nextLine();

                if (loginUser(connection, username, inputPassword)) {
                    System.out.println("Inicio de sesión exitoso.");
                } else {
                    System.out.println("Credenciales incorrectas. No se pudo iniciar sesión.");
                }
            } else {
                System.out.println("Opción no válida.");
            }
        } catch (SQLException e) {
            System.err.println("Error al conectar a la base de datos: " + e.getMessage());
        }
    }

    public static boolean isUserRegistered(Connection connection, String username) throws SQLException {
        String query = "SELECT * FROM usuarios WHERE nombre = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, username);
        ResultSet resultSet = preparedStatement.executeQuery();

        return resultSet.next();
    }

    public static void registerUser(Connection connection, String username, String password) throws SQLException {
        String query = "INSERT INTO usuarios (nombre, contraseña) VALUES (?, ?)";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, username);
        preparedStatement.setString(2, password);
        preparedStatement.executeUpdate();
        System.out.println("Cuenta creada exitosamente.");
    }

    public static boolean loginUser(Connection connection, String username, String password) throws SQLException {
        String query = "SELECT * FROM usuarios WHERE nombre = ? AND contraseña = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, username);
        preparedStatement.setString(2, password);
        ResultSet resultSet = preparedStatement.executeQuery();

        return resultSet.next();
    }
}
